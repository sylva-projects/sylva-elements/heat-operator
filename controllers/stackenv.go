/*
Copyright 2023. The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


Derived work from https://github.com/gophercloud/gophercloud/tree/master/openstack/orchestration/v1
Copyright Gophercloud authors
*/

package controllers

import (
	"github.com/gophercloud/gophercloud"
)

// Theses functions permit to retrieve environment of a HeatStack since its not possible via GopherCloud module

type GetResult struct {
	gophercloud.Result
}

type EnvironmentR struct {
	Parameters        map[string]interface{} `json:"parameters,omitempty"`
	ParameterDefaults map[string]interface{} `json:"parameter_defaults,omitempty"`
}

/*
The OpenStack api returns an empty struct (Environment:{ parameters:{}, parameter_defaults:{} })
when the stack has no environment parameters.
To match this behavior, "environment.Parameters" and "environment.ParametersDefault" are initialize as map[string]interface{}{}
*/
func NewEnvironment() EnvironmentR {
	environment := EnvironmentR{}
	environment.ParameterDefaults = map[string]interface{}{}
	environment.Parameters = map[string]interface{}{}
	return environment
}

func GetEnvFromHeatStack(c *gophercloud.ServiceClient, stackName, stackID string) (r GetResult) {
	env_req := c.ServiceURL("stacks", stackName, stackID, "environment")
	resp, err := c.Get(env_req, &r.Body, nil)
	_, r.Header, r.Err = gophercloud.ParseResponse(resp, err)
	return
}

func (r GetResult) Extract() (EnvironmentR, error) {
	var e EnvironmentR
	if r.Err != nil {
		return e, r.Err
	}
	err := r.ExtractInto(&e)
	return e, err
}
