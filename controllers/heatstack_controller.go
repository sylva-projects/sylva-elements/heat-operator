/*
Copyright 2023. The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	"github.com/go-logr/logr"
	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/orchestration/v1/stacks"
	"github.com/gophercloud/gophercloud/openstack/orchestration/v1/stacktemplates"
	heatoperatorv1 "gitlab.com/sylva-projects/sylva-elements/heat-operator/api/v1"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

// HeatStackReconciler reconciles a Stack object
type HeatStackReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type StackError struct {
	Code        int          `json:"code"`
	Title       string       `json:"title"`
	Explanation string       `json:"explanation"`
	Error       UnifiedError `json:"error"`
}

type UnifiedError struct {
	Type    string `json:"type"`
	Message string `json:"message"`
}

var ignored_actions = [...]string{"ROLLBACK", "SUSPEND", "RESUME", "ADOPT", "SNAPSHOT", "CHECK"}

const finalizerName = "heatoperator.sylva/finalizer"

// +kubebuilder:rbac:groups=heatoperator.sylva,resources=heatstacks,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=heatoperator.sylva,resources=heatstacks/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=heatoperator.sylva,resources=heatstacks/finalizers,verbs=update
// +kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;delete
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;

func (r *HeatStackReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logs := r.Log.WithValues("HeatStack", req.NamespacedName)
	stack := &heatoperatorv1.HeatStack{}
	forceUpdate := false
	stacksEqual := true
	err := r.Get(ctx, req.NamespacedName, stack)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Return and don't requeue
			logs.Info("HeatStack resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		logs.Error(err, "Failed to get HeatStack")
		return ctrl.Result{}, err
	}

	//Define orchestrationClient
	//client *gophercloud.ServiceClient
	osclient, err := r.getOrchestrationClient(ctx, stack)
	if err != nil {
		logs.Error(err, "Failed to initialize Orchestration client", "HeatStack.name", stack.Name)
		return ctrl.Result{}, err
	}

	//Check if Stack already exist on OpenStack
	stackName := r.determineStackName(ctx, stack)
	heatStack, err := stacks.Find(osclient, stackName).Extract()
	if err != nil {
		_, err404 := err.(gophercloud.ErrDefault404)
		if err404 {
			if stack.Status.StackID == "" {
				// Create the Heat stack only if the Heat stack is not found on OpenStack side
				// and if StackID is not present in stackCR status
				if err = createHeatStack(osclient, stack, stackName, logs); err != nil {
					logs.Error(err, "Failed to create HeatStack", "HeatStack.namespace", stack.Namespace, "HeatStack.name", stack.Name)
					if r.isValidationFailed(ctx, err, stack, "", stackName, logs) {
						// Stop the reconciliation after StackValidationFailed
						return ctrl.Result{}, nil
					}
				} else {
					//Create successfully pursue the reconcile cycle
					return ctrl.Result{RequeueAfter: stack.Spec.Interval.Duration}, nil
				}
			} else if stack.GetDeletionTimestamp() != nil && controllerutil.ContainsFinalizer(stack, finalizerName) {
				if err = r.removeFinalizerAndCM(ctx, logs, stack); err != nil {
					return ctrl.Result{RequeueAfter: stack.Spec.RetryInterval.Duration}, err
				}
				return ctrl.Result{}, nil
			}
		}
		return ctrl.Result{}, err
	} else {
		if stack.GetDeletionTimestamp() != nil && controllerutil.ContainsFinalizer(stack, finalizerName) {
			// nolint:errcheck
			r.updateStatus(stack, ctx, heatStack.Status, heatStack.StatusReason, heatStack.ID, stackName, logs)
			if heatStack.Status != "DELETE_IN_PROGRESS" {
				if heatStack.Status == "DELETE_FAILED" {
					logs.Error(fmt.Errorf("%s", heatStack.StatusReason), heatStack.Status, stack.Name, stack.Namespace)
				}
				// Delete HeatStack
				if err := r.deleteHeatStack(ctx, logs, osclient, stack, heatStack.ID, stackName); err != nil {
					logs.Error(err, "Failed to delete HeatStack", "HeatStack.namespace", stack.Namespace, "HeatStack.name", stack.Name)
					return ctrl.Result{}, err
				}
			}
			if heatStack.Status == "DELETE_FAILED" {
				return ctrl.Result{RequeueAfter: stack.Spec.RetryInterval.Duration}, nil
			} else {
				return ctrl.Result{RequeueAfter: stack.Spec.Interval.Duration}, nil
			}
		}

		// Add finalizer to HeatStack
		if !controllerutil.ContainsFinalizer(stack, finalizerName) {
			controllerutil.AddFinalizer(stack, finalizerName)
			if err = r.Update(ctx, stack); err != nil {
				logs.Error(err, "Failed to add finalizer on HeatStack", "HeatStack.namespace", stack.Namespace, "HeatStack.name", stack.Name)
				return ctrl.Result{}, err
			}
			return ctrl.Result{}, nil
		}

		// Stack exist but his status is failed, let's try to recreate it via stack update
		if heatStack.Status == "CREATE_FAILED" || heatStack.Status == "UPDATE_FAILED" {
			logs.Error(fmt.Errorf("%s", heatStack.StatusReason), heatStack.Status, stack.Name, stack.Namespace)
			stack.Status.FailureMessage = heatStack.StatusReason
			forceUpdate = true
		} else {
			// Stack is in a good state, check if an update is necessary
			stacksEqual, err = stacksEquals(osclient, stack, heatStack.ID, logs)
			if err != nil {
				if errstatus := r.updateStatus(stack, ctx, "INTERNAL_ERROR", fmt.Sprintf("%s", err), heatStack.ID, stackName, logs); errstatus != nil {
					logs.Error(errstatus, "Failed to update HeatStack status", "HeatStack.namespace", stack.Namespace, "HeatStack.name", stack.Name)
					return ctrl.Result{}, errstatus
				}
				return ctrl.Result{}, err
			}
		}

		if !stacksEqual || forceUpdate {
			if err = updateHeatStack(osclient, stack, heatStack.ID, stackName, logs); err != nil {
				logs.Error(err, "Failed to update HeatStack", "HeatStack.namespace", stack.Namespace, "HeatStack.name", stack.Name)
				if r.isValidationFailed(ctx, err, stack, heatStack.ID, stackName, logs) {
					//Stop the reconciliation due to StackValidation
					return ctrl.Result{}, nil
				} else {
					return ctrl.Result{RequeueAfter: stack.Spec.RetryInterval.Duration}, err
				}
			}
		}

		//Update stackStatus and update configmap to store stack output
		requeue, err := r.checkStatusAndOuput(ctx, osclient, stack, heatStack.ID, logs)
		if err != nil {
			logs.Error(err, "Failed to update status of HeatStack", "HeatStack.namespace", stack.Namespace, "HeatStack.name", stack.Name)
			return ctrl.Result{}, err
		}

		if requeue {
			if forceUpdate {
				return ctrl.Result{RequeueAfter: stack.Spec.RetryInterval.Duration}, nil
			} else {
				return ctrl.Result{RequeueAfter: stack.Spec.Interval.Duration}, nil
			}
		}

		return ctrl.Result{}, nil
	}
}

// Update the status according to the error
// and stop or continue the reconcile cycle
func (r *HeatStackReconciler) isValidationFailed(ctx context.Context, err error, stackCR *heatoperatorv1.HeatStack, stackId string, stackName string, logs logr.Logger) bool {
	var jsonResponse StackError
	if err400, ok := err.(gophercloud.ErrDefault400); ok {
		// nolint:errcheck
		json.Unmarshal(err400.Body, &jsonResponse)
		// nolint:errcheck
		r.updateStatus(stackCR, ctx, jsonResponse.Error.Type, jsonResponse.Error.Message, stackId, stackName, logs)
		if jsonResponse.Error.Type == "StackValidationFailed" {
			// Stop the reconcile cycle
			return true
		}
	}
	return false
}

func (r *HeatStackReconciler) deleteHeatStack(ctx context.Context, logs logr.Logger, osclient *gophercloud.ServiceClient, stackCR *heatoperatorv1.HeatStack, stackId string, stackName string) error {
	logs.Info("deleteHeatStack", "HeatStack.name", stackCR.Name)
	// Delete Heat stack on OpenStack
	if stack_delete := stacks.Delete(osclient, stackName, stackId); stack_delete.Err != nil {
		logs.Error(stack_delete.Err, "Unable to delete HeatStack on OpenStack", "StackName", stackCR.Name)
		return stack_delete.Err
	}
	logs.Info("deleteHeatStack", "Stack deleted successfully", stackCR.Name)
	return nil
}

func (r *HeatStackReconciler) removeFinalizerAndCM(ctx context.Context, logs logr.Logger, stackCR *heatoperatorv1.HeatStack) error {
	logs.Info("removeFinalizerAndCM", "HeatStack.name", stackCR.Name)

	// Remove associated configmap
	var config = &corev1.ConfigMap{}
	cmConfig := client.ObjectKey{
		Name:      stackCR.Spec.OutputConfigMap.Name,
		Namespace: stackCR.Namespace,
	}
	// Retrieve the configMap
	if err := r.Get(ctx, cmConfig, config); err != nil {
		return err
	}
	// Delete the configmap
	if err := r.Delete(ctx, config); err != nil {
		return err
	}

	//Remove Finalizer on stack CR
	controllerutil.RemoveFinalizer(stackCR, finalizerName)
	if err := r.Update(ctx, stackCR); err != nil {
		return err
	}
	return nil
}

func (r *HeatStackReconciler) checkStatusAndOuput(ctx context.Context, osclient *gophercloud.ServiceClient, stackCR *heatoperatorv1.HeatStack, stackId string, logs logr.Logger) (bool, error) {
	logs.Info("checkStatusAndOuput", "HeatStack.name", stackCR.Name)
	requeue := true
	// Update status
	getStack, err := stacks.Find(osclient, stackId).Extract()
	if err != nil {
		return requeue, err
	}

	// Creation of the configmap to store the stack output
	form := make(map[string]string)
	for _, output := range getStack.Outputs {
		//cast interface{} to string
		output_key := fmt.Sprint(output["output_key"])
		output_value := fmt.Sprint(output["output_value"])
		form[output_key] = output_value
	}
	// Add Stack Name in the configmap
	form["stackName"] = getStack.Name

	configMap := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      stackCR.Spec.OutputConfigMap.Name,
			Namespace: stackCR.Namespace,
			Labels:    stackCR.Spec.OutputConfigMap.Labels,
		},
		Data: form,
	}

	cmObject := client.ObjectKey{
		Name:      stackCR.Spec.OutputConfigMap.Name,
		Namespace: stackCR.Namespace,
	}
	err = r.Get(ctx, cmObject, &corev1.ConfigMap{})
	if k8serrors.IsNotFound(err) {
		// Create the configmap
		if err = r.Create(ctx, configMap); err != nil {
			return requeue, err
		}
	} else {
		// Update the configmap
		if err = r.Update(ctx, configMap); err != nil {
			return requeue, err
		}
	}

	if err = r.updateStatus(stackCR, ctx, getStack.Status, getStack.StatusReason, stackId, getStack.Name, logs); err != nil {
		return requeue, err
	}

	if getStack.Status == "UPDATE_IN_PROGRESS" || getStack.Status == "CREATE_IN_PROGRESS" {
		// Stack still in progress pursue the Reconcile
		return requeue, nil
	}

	return false, nil

}

func updateHeatStack(osclient *gophercloud.ServiceClient, stackCR *heatoperatorv1.HeatStack, stackId string, stackName string, logs logr.Logger) error {
	logs.Info("updateHeatStack", "Stack update", stackCR.Name)
	template, err := getStackTemplate(stackCR)
	if err != nil {
		return err
	}
	environment, err := getStackEnvironment(stackCR)
	if err != nil {
		return err
	}

	tags := []string{stackCR.Spec.HeatStack.Tag}
	stackOpts := &stacks.UpdateOpts{
		TemplateOpts:    template,
		EnvironmentOpts: environment,
		Tags:            tags,
	}
	stack_update := stacks.Update(osclient, stackName, stackId, stackOpts)
	if stack_update.Err != nil {
		logs.Error(stack_update.Err, "Unable to update HeatStack on OpenStack", "StackName", stackCR.Name)
		return stack_update.Err
	}

	logs.Info("updateHeatStack", "Update of Heat stack successfully sent to OpenStack", stackCR.Name)
	return nil
}

func createHeatStack(osclient *gophercloud.ServiceClient, stackCR *heatoperatorv1.HeatStack, stackName string, logs logr.Logger) error {
	logs.Info("createHeatStack", "Stack create", stackCR.Name)

	template, err := getStackTemplate(stackCR)
	if err != nil {
		return err
	}
	environment, err := getStackEnvironment(stackCR)
	if err != nil {
		return err
	}

	tags := []string{stackCR.Spec.HeatStack.Tag}
	createOpts := &stacks.CreateOpts{
		Name:            stackName,
		TemplateOpts:    template,
		EnvironmentOpts: environment,
		Tags:            tags,
	}

	stack_create := stacks.Create(osclient, createOpts)
	if stack_create.Err != nil {
		logs.Error(stack_create.Err, "Unable to create Heat stack on OpenStack", "StackName", stackCR.Name)
		return stack_create.Err
	}
	return nil
}

func stacksEquals(osclient *gophercloud.ServiceClient, stackCR *heatoperatorv1.HeatStack, stackID string, logs logr.Logger) (bool, error) {
	var template_from_cr map[string]interface{}
	var template_from_heat map[string]interface{}

	// Get Heat tag from OpenStack
	getStack, err := stacks.Find(osclient, stackID).Extract()
	if err != nil {
		logs.Error(err, "Cannot retrieve Stack from OpenStack", "HeatStack.name", stackCR.Name)
		return false, err
	}
	stackName := getStack.Name

	// Get Heat template from CR object
	templatedata, err := getStackTemplate(stackCR)
	if err != nil {
		logs.Error(err, "Cannot retrieve template from HeatStack CR", "HeatStack.name", stackCR.Name)
		return false, err
	}

	if err = json.Unmarshal(templatedata.TE.Bin, &template_from_cr); err != nil {
		return false, err
	}

	// Get Heat environment from CR object
	var env_from_cr = NewEnvironment()
	if stackCR.Spec.HeatStack.Environment != nil {
		environment, err := getStackEnvironment(stackCR)
		if err != nil {
			logs.Error(err, "Cannot retrieve environment from HeatStack CR", "HeatStack.name", stackCR.Name)
			return false, err
		}
		if err = json.Unmarshal(environment.TE.Bin, &env_from_cr); err != nil {
			return false, err
		}
	}

	// Get Heat tag from CR object
	tag_from_cr := []string{stackCR.Spec.HeatStack.Tag}

	// Get Heat template from OpenStack
	tmpl, err := stacktemplates.Get(osclient, stackName, stackID).Extract()
	if err != nil {
		logs.Error(err, "Cannot retrieve Heat template from OpenStack", "HeatStack.name", stackCR.Name)
		return false, err
	}
	if err = json.Unmarshal([]byte(tmpl), &template_from_heat); err != nil {
		return false, err
	}

	// Get Heat environment from OpenStack
	env_from_heat, err := GetEnvFromHeatStack(osclient, stackName, stackID).Extract()
	if err != nil {
		logs.Error(err, "Cannot retrieve Heat environment template from OpenStack", "HeatStack.name", stackCR.Name)
		return false, err
	}

	// Compare template, environment, tag values from Heat with template,environment and tag values from CustomResource
	equal := reflect.DeepEqual(template_from_cr, template_from_heat) && reflect.DeepEqual(env_from_cr, env_from_heat) && reflect.DeepEqual(tag_from_cr, getStack.Tags)

	return equal, nil

}

func (r *HeatStackReconciler) updateStatus(stackCR *heatoperatorv1.HeatStack, ctx context.Context, status string, message string, stackID string, stackName string, logs logr.Logger) error {
	logs.Info("updateStatus", "HeatStack.name", stackCR.Name, "HeatStack.status", status, "HeatStack.message", message)

	if !ignoredAction(status) {
		if status == "UPDATE_COMPLETE" || status == "CREATE_COMPLETE" {
			stackCR.Status.ObservedGeneration = stackCR.Generation //Set status.ObservedGeneration only when the stack is ready
			stackCR.Status.FailureMessage = ""
			stackCR.Status.SetReadyCondition(status, message, stackCR.Generation)
		} else if status == "UPDATE_IN_PROGRESS" || status == "CREATE_IN_PROGRESS" || status == "DELETE_IN_PROGRESS" {
			stackCR.Status.SetReconcilingCondition(status, message, stackCR.Generation)
		} else {
			stackCR.Status.FailureMessage = message
			stackCR.Status.SetStalledCondition(status, message, stackCR.Generation)
		}
	}

	stackCR.Status.StackID = stackID
	stackCR.Status.StackName = stackName
	if err := r.Status().Update(ctx, stackCR); err != nil {
		return err
	}

	return nil
}

func (r *HeatStackReconciler) determineStackName(ctx context.Context, stackCR *heatoperatorv1.HeatStack) string {
	// Retrieving the stack name from the status
	// if not present retrieve the stack name from the configmap
	// else generate stack name based on the prefix and the first five characters of the UID

	// StackName present in status
	if stackCR.Status.StackName != "" {
		return stackCR.Status.StackName
	}

	// Generate a StackName
	stackNameGenerated := stackCR.Spec.HeatStack.NamePrefix + "-" + string(stackCR.UID[0:5])

	// StackName present in configMap
	cmObject := client.ObjectKey{
		Name:      stackCR.Spec.OutputConfigMap.Name,
		Namespace: stackCR.Namespace,
	}
	var config = &corev1.ConfigMap{}
	err := r.Get(ctx, cmObject, config)
	if err != nil {
		return stackNameGenerated
	}
	if config.Data["stackName"] != "" {
		return config.Data["stackName"]
	}

	return stackNameGenerated
}

func getStackTemplate(stackCR *heatoperatorv1.HeatStack) (*stacks.Template, error) {
	template := &stacks.Template{}

	// Template struct
	templatedata, err := json.Marshal(stackCR.Spec.HeatStack.Template)
	if err != nil {
		return template, err
	}
	template.TE = stacks.TE{
		Bin: templatedata,
	}

	return template, nil
}

func getStackEnvironment(stackCR *heatoperatorv1.HeatStack) (*stacks.Environment, error) {
	environment := &stacks.Environment{}

	// Environment struct
	environmentdata, err := json.Marshal(stackCR.Spec.HeatStack.Environment)
	if err != nil {
		return environment, err
	}
	environment.TE = stacks.TE{
		Bin: environmentdata,
	}
	return environment, nil
}

func ignoredAction(currentStatus string) bool {
	for _, ignored := range ignored_actions {
		if strings.HasPrefix(currentStatus, ignored) {
			return true
		}
	}
	return false
}

// SetupWithManager sets up the controller with the Manager.
func (r *HeatStackReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&heatoperatorv1.HeatStack{}).Complete(r)
}
