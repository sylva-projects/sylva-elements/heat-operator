/*
Copyright 2023. The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


Derived work from https://github.com/kubernetes-sigs/cluster-api-provider-openstack/blob/main/pkg/scope/provider
Copyright 2020 The Kubernetes authors
*/

package controllers

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"net/http"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/gophercloud/utils/openstack/clientconfig"
	heatoperatorv1 "gitlab.com/sylva-projects/sylva-elements/heat-operator/api/v1"
	"gopkg.in/yaml.v2"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
)

func (r *HeatStackReconciler) getOrchestrationClient(ctx context.Context, stackCR *heatoperatorv1.HeatStack) (*gophercloud.ServiceClient, error) {
	sClient := &gophercloud.ServiceClient{}
	var cloud clientconfig.Cloud
	var caCert []byte
	cloud, caCert, err := r.getCloudFromSecret(ctx, stackCR.Spec.IdentityRef, stackCR.Namespace, stackCR.Spec.CloudName)
	if err != nil {
		return sClient, err
	}
	// nolint:ineffassign
	pClient := &gophercloud.ProviderClient{}
	pClient, err = newOSClient(cloud, caCert)
	if err != nil {
		return sClient, err
	}
	eo := gophercloud.EndpointOpts{}
	return openstack.NewOrchestrationV1(pClient, eo)
}

func (r *HeatStackReconciler) getCloudFromSecret(ctx context.Context, secretName string, secretNamespace string, cloudName string) (clientconfig.Cloud, []byte, error) {
	cloudFileKey := "clouds.yaml"
	caFileKey := "cacert"
	emptyCloud := clientconfig.Cloud{}

	secret := &corev1.Secret{}
	err := r.Get(ctx, types.NamespacedName{
		Namespace: secretNamespace,
		Name:      secretName,
	}, secret)
	if err != nil {
		return emptyCloud, nil, err
	}

	content, ok := secret.Data[cloudFileKey]
	if !ok {
		err := errors.New("OpenStack credentials secret did not contain key cloud.yaml")
		return emptyCloud, nil, err
	}
	var clouds clientconfig.Clouds
	if err = yaml.Unmarshal(content, &clouds); err != nil {
		return emptyCloud, nil, err
	}

	// get caCert
	caCert, ok := secret.Data[caFileKey]
	if !ok {
		return clouds.Clouds[cloudName], nil, nil
	}

	return clouds.Clouds[cloudName], caCert, nil
}

func newOSClient(cloud clientconfig.Cloud, caCert []byte) (*gophercloud.ProviderClient, error) {
	clientOpts := new(clientconfig.ClientOpts)
	if cloud.AuthInfo != nil {
		clientOpts.AuthInfo = cloud.AuthInfo
		clientOpts.AuthType = cloud.AuthType
		clientOpts.RegionName = cloud.RegionName
	}

	opts, err := clientconfig.AuthOptions(clientOpts)
	if err != nil {
		return nil, err
	}
	opts.AllowReauth = true

	provider, err := openstack.NewClient(opts.IdentityEndpoint)
	if err != nil {
		return nil, err
	}

	config := &tls.Config{
		MinVersion: tls.VersionTLS12,
	}
	if cloud.Verify != nil {
		config.InsecureSkipVerify = !*cloud.Verify
	}
	if caCert != nil {
		// Only create a specific pool if custom CA pool is given
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		config.RootCAs = caCertPool
	}

	provider.HTTPClient.Transport = &http.Transport{Proxy: http.ProxyFromEnvironment, TLSClientConfig: config}

	err = openstack.Authenticate(provider, *opts)
	if err != nil {
		return nil, err
	}

	return provider, nil
}
