/*
Copyright 2023. The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"encoding/json"

	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type UnstructuredDataType struct {
	// +kubebuilder:pruning:PreserveUnknownFields
	unstructured.Unstructured `json:",inline"`
}

type StackInfoSpec struct {

	// Prefix used to build the stackName
	// +optional
	// +kubebuiler:validation:Optional
	// +kubebuilder:validation:Pattern="^[a-zA-Z][a-zA-Z0-9-_]*[a-zA-Z0-9]$"
	// +kubebuilder:default=heat-operator
	NamePrefix string `json:"namePrefix"`
	// Template of the stack
	// +optional
	// +kubebuilder:validation:Required
	Template *UnstructuredDataType `json:"template"`
	// Environment values of the stack
	// +optional
	// +kubebuilder:validation:Optional
	Environment *UnstructuredDataType `json:"environment,omitempty"`
	// Tag associate to Heat Stack
	// +optional
	// +kubebuilder:validation:Required
	Tag string `json:"tag"`
}

type ConfigMapInfoSpec struct {

	// Name of the configmap which will contain the stack output
	// +optional
	// +kubebuilder:validation:Required
	Name string `json:"name"`
	// Label added to the configmap
	// +optional
	// +kubebuilder:validation:Required
	Labels map[string]string `json:"labels"`
}

// HeatStackSpec defines the desired state of Stack
type HeatStackSpec struct {

	// Ref to kubernetes cloud-config secret
	// +optional
	// +kubebuilder:validation:Required
	IdentityRef string `json:"identityRef"`
	// Name of the cloud that will be used in identityRef
	// +optional
	// +kubebuilder:validation:Required
	CloudName string `json:"cloudName"`
	// The interval at which to reconcile the HeatStack.
	// +kubebuilder:validation:Type=string
	// +kubebuilder:validation:Pattern="^([0-9]+(\\.[0-9]+)?(ms|s|m|h))+$"
	// +kubebuilder:default="5000ms"
	// +optional
	Interval metav1.Duration `json:"interval,omitempty"`
	// The interval at which to retry a previously failed reconciliation.
	// +kubebuilder:validation:Type=string
	// +kubebuilder:validation:Pattern="^([0-9]+(\\.[0-9]+)?(ms|s|m|h))+$"
	// +kubebuilder:default="10m"
	// +optional
	RetryInterval metav1.Duration `json:"retryInterval,omitempty"`
	// +required
	// +kubebuilder:validation:Required
	HeatStack StackInfoSpec `json:"heatStack"`
	// +optional
	// +kubebuilder:validation:Required
	OutputConfigMap ConfigMapInfoSpec `json:"outputConfigMap"`
}

// HeatStackStatus defines the observed state of Stack
type HeatStackStatus struct {
	Conditions         []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type" protobuf:"bytes,1,rep,name=conditions"`
	FailureMessage     string             `json:"failureMessage,omitempty"`
	StackID            string             `json:"stackid,omitempty"`
	StackName          string             `json:"stackname,omitempty"`
	ObservedGeneration int64              `json:"observedGeneration,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Ready",type="string",JSONPath=`.status.conditions[?(@.type=="Ready")].status`
// +kubebuilder:printcolumn:name="Status",type="string",JSONPath=`.status.conditions[?(@.type=="Ready")].reason`
// +kubebuilder:printcolumn:name="StackName",type="string",JSONPath=`.status.stackname`
// +kubebuilder:printcolumn:name="StackID",type="string",JSONPath=`.status.stackid`

// HeatStack is the Schema for the stacks API
type HeatStack struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec HeatStackSpec `json:"spec,omitempty"`
	// +kubebuilder:default:={"observedGeneration":-1}
	Status HeatStackStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// HeatStackList contains a list of HeatStack
type HeatStackList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []HeatStack `json:"items"`
}

// The conditions respect the kstatus spec (https://github.com/kubernetes-sigs/cli-utils/blob/master/pkg/kstatus/README.md), as follows:
// - while the HeatStack is being processed, the condition `Reconciling` will be present with a value of `True` and the condition `Ready` will be present with a value of `False`
// - if the HeatStack failed, the condition `Stalled` will be present with a value of ` True` and the condition `Ready` will be present with a value of `False`
// - if the HeatStack is completed, the condition `Ready` will be present with a value of `True`

const (
	ReadyCondition       = "Ready"
	StalledCondition     = "Stalled"
	ReconcilingCondition = "Reconciling"
)

func (s *HeatStackStatus) SetReconcilingCondition(reason string, msg string, generation int64) {
	conditions := &s.Conditions
	apimeta.RemoveStatusCondition(conditions, StalledCondition)
	apimeta.SetStatusCondition(conditions, metav1.Condition{
		Type:               ReadyCondition,
		Status:             "False",
		Reason:             reason,
		Message:            msg,
		ObservedGeneration: generation,
		LastTransitionTime: metav1.Now(),
	})
	apimeta.SetStatusCondition(conditions, metav1.Condition{
		Type:               ReconcilingCondition,
		Status:             "True",
		Reason:             reason,
		Message:            msg,
		ObservedGeneration: generation,
		LastTransitionTime: metav1.Now(),
	})
}

func (s *HeatStackStatus) SetStalledCondition(reason string, msg string, generation int64) {
	conditions := &s.Conditions
	apimeta.RemoveStatusCondition(conditions, ReconcilingCondition)
	apimeta.SetStatusCondition(conditions, metav1.Condition{
		Type:               ReadyCondition,
		Status:             "False",
		Reason:             reason,
		Message:            msg,
		ObservedGeneration: generation,
		LastTransitionTime: metav1.Now(),
	})
	apimeta.SetStatusCondition(conditions, metav1.Condition{
		Type:               StalledCondition,
		Status:             "True",
		Reason:             reason,
		Message:            msg,
		ObservedGeneration: generation,
		LastTransitionTime: metav1.Now(),
	})
}

func (s *HeatStackStatus) SetReadyCondition(reason string, msg string, generation int64) {
	conditions := &s.Conditions
	apimeta.RemoveStatusCondition(conditions, ReconcilingCondition)
	apimeta.RemoveStatusCondition(conditions, StalledCondition)
	apimeta.SetStatusCondition(conditions, metav1.Condition{
		Type:               ReadyCondition,
		Status:             "True",
		Reason:             reason,
		Message:            msg,
		ObservedGeneration: generation,
		LastTransitionTime: metav1.Now(),
	})
}

// UnstructuredDataType functions
func (in *UnstructuredDataType) DeepCopyInto(out *UnstructuredDataType) {
	//controller-gen cannot handle the interface{} type of an aliased Unstructured,
	//thus we write our own DeepCopyInto function.
	if out != nil {
		casted := unstructured.Unstructured(in.Unstructured)
		deepCopy := casted.DeepCopy()
		out.Object = deepCopy.Object
	}
}

func (in *UnstructuredDataType) UnmarshalJSON(data []byte) error {
	m := make(map[string]interface{})
	if err := json.Unmarshal(data, &m); err != nil {
		return err
	}

	in.Object = m

	return nil
}

func (in *UnstructuredDataType) MarshalJSON() (data []byte, err error) {
	return json.Marshal(in.Object)
}

func init() {
	SchemeBuilder.Register(&HeatStack{}, &HeatStackList{})
}
